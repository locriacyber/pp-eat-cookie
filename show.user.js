// ==UserScript==
// @name     Show cookie at top
// @description Display cookie at page top as a tiny line
// @version  1
// @grant    none
// @run-at	 document-start
// @match    https://pixelplace.io/*
// ==/UserScript==

const _class = "clfglfgcroeaodh"

setInterval(() => {
  const el = document.querySelector('body')
  if (!el) return

  let el1 = document.getElementsByClassName(_class)[0]
  if (!el1) {  
    el1 = document.createElement('input')
    el1.type = "text"
  	el1.value = document.cookie
    el1.classList.add(_class)
    el1.style = "line-height: 1; font-size: 0.3rem; position: absolute; top: 0;left: 0;width: 100vw;z-index: 100;"
    el.insertAdjacentElement("afterbegin", el1)
  }
  el1.value = document.cookie
}, 1000)